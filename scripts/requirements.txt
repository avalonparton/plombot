# Plombot pip requirements
# discord + pre-reqs
discord.py[voice]
asyncio
PyNaCl

# Async http requests
aiohttp[speedups]

# Database
pony

# APIs
dblpy        # Discord Bot List
lyricsgenius # Genius
spotipy      # Spotify
youtube-dl   # YouTube
isodate      # - parsing YouTube video durations
opendota2py  # OpenDota

# Testing
pytest
pytest-asyncio

import asyncio
from messages import en
import re

languages = {}
for lang in [en]:
    languages[lang.language] = lang.translations


# regex
keywords = [
    "official audio",
    "official video",
    "official music audio",
    "official music video",
]
title_format = re.compile('|'.join(keywords), re.IGNORECASE)


def translate(ctx, message, args=[]):
    language = ctx.language.lower()

    # Lookup translation in dictionary
    try:
        translation = languages[language][message]
    except KeyError:
        print(f"Missing {language} translation for {message}")
        return None

    # Convert lists to strings.
    if type(languages[language][message]) is list:
        translation = "".join(translation)

    # Replace keywords with arguments.
    return translation.format(*args)


async def send(ctx, message, args=[]):
    """ Translates and sends a message. """
    translation = translate(ctx, message, args)
    if translation is None:
        translation = f"Missing translation for `{message}`` in **{ctx.language}**. Join the [Discord](https://discord.gg/Czj2g9c) and ask how you could help with translations!"
    await ctx.send(translation)


def format_duration(duration) -> str:
    """ Converts a duration into a string like '4h20m' """
    if duration is None:
        return "?"
    if duration < 60:  # Under a minute
        return "{}s".format(int(duration))
    if duration < 3600:  # Under an hour
        return "{}m{}s".format(int(duration / 60), int(duration % 60))
    # Over an hour
    return "{}h{}m{}s".format(int(duration / 3600), int(duration % 3600 / 60), int(duration % 60))


def format_title(title) -> str:
    """ Removes "official audio/video" etc from video titles """
    return title_format.sub('', title).strip()

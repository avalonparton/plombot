""" Admin commands extension """
import discord
from discord.ext import commands
from cogs import messaging


def get_announce_channel(guild):
    """ Returns a channel to send the announcement to, or None """
    # Look for channels with these names.
    preferred_channel_names = ["general", "chat", "music", "bot"]
    for preferred_channel_name in preferred_channel_names:
        for text_channel in guild.text_channels:
            if text_channel.name == preferred_channel_name:
                if guild.me.permissions_in(text_channel).send_messages:
                    return text_channel

    # Find the first channel with write permission.
    for text_channel in guild.text_channels:
        if guild.me.permissions_in(text_channel).send_messages:
            return text_channel

    # Failed to find a channel :(
    return None

# Checks


async def author_is_plomdawg(ctx):
    """ Returns True if the author is plomdawg """
    return ctx.author.id == 163040232701296641


class Admin(commands.Cog):
    """ Admin cog

    Provides commands:
         ;help ;reload ;vote
    """

    def __init__(self, bot):
        self.bot = bot
        self.bot.remove_command('help')

    @ commands.command(aliases=["r"])
    @ commands.check(author_is_plomdawg)
    async def reload(self, ctx):
        """ Reloads all cogs """
        async with ctx.typing():
            ctx.bot.reload_extension('cogs.admin')
            ctx.bot.reload_extension('cogs.database')
            ctx.bot.reload_extension('cogs.dota')
            ctx.bot.reload_extension('cogs.error_handler')
            ctx.bot.reload_extension('cogs.youtube')
            ctx.bot.reload_extension('cogs.music')
            await ctx.send("Reloaded cogs.")

    @ commands.command(aliases=["?"])
    async def help(self, ctx):
        """Sends help message """
        await self.bot.send_help(ctx=ctx, prefix=ctx.prefix)

    @ commands.command()
    async def donate(self, ctx):
        """ Sends crypto wallets """
        async with ctx.typing():
            embed = discord.Embed(
                title="Donate to keep plombot alive and free!")
            embed.add_field(
                name="CASH DOLLARS", value="[buymeacoffee.com/plom](https://www.buymeacoffee.com/plom)", inline=False)
            embed.add_field(
                name="ETH", value="0xbb7ab8c1b005e95a350Cd650DDBF4c2f89eEf41a", inline=False)
            embed.add_field(
                name="DOGE", value="DCVniSYuarfBrGTZQsLX8mSdSJdtrCdZa7", inline=False)
            await ctx.send(embed=embed)

    @ commands.check(author_is_plomdawg)
    @ commands.command()
    async def servers(self, ctx, *args):
        """ Prints info about all connected servers """
        async with ctx.typing():
            # Construct a string of text to send.
            text = ""

            # Print info for each guild.
            for guild in ctx.bot.guilds:
                # Guild name.
                text += f"** --------------- {guild.name} --------------- **\n"

                # Member count excluding self.
                num_users = guild.member_count - 1
                text += f" - **Population:** {num_users} users\n"

                # Text channels.
                text += f" - **Text channels:**  {len(guild.text_channels)}\n"
                # for text_channel in guild.text_channels:
                #    if text_channel.id != 828650535203635220:
                #        if guild.me.permissions_in(text_channel).send_messages:
                #            emoji = "✅"
                #        else:
                #            emoji = "❌"
                #        text += f" - - - {emoji} {text_channel.name}\n"

                # Voice channels.
                text += f" - **Voice channels:** {len(guild.voice_channels)}\n"
                # for voice_channel in guild.voice_channels:
                #    if guild.me.permissions_in(voice_channel).speak:
                #        emoji = "✅"
                #    else:
                #        emoji = "❌"
                #    text += f" - - - {emoji} {voice_channel.name}\n"

            return await ctx.bot.send_embed(ctx, title="Servers", text=text)

    @ commands.check(author_is_plomdawg)
    @ commands.command()
    async def announce(self, ctx, *args):
        """ Sends a message to all servers """

        if len(args) == 0:
            return await ctx.send(f"```Usage: {ctx.prefix}announce [message]```")

        offset = len(ctx.prefix) + 8  # 8 = len("announce")
        message = ctx.message.content[offset:]

        # Add link to support server to the message
        message += "\n\n([Join the support server](https://discord.gg/Czj2g9c) to respond)"

        async with ctx.typing():
            # Construct a string of text to send.
            text = f"Sending message to **{len(ctx.bot.guilds)}** servers:\n"
            footer = f"Sent by {ctx.message.author.display_name}"
            footer_icon = ctx.message.author.avatar_url_as(size=64)
            title = "👋"

            text += f"**guild**: [text channel]\n\n"
            for guild in ctx.bot.guilds:
                announce_channel = get_announce_channel(guild)

                # Try to send the message to the channel.
                if await ctx.bot.send_embed(announce_channel, text=message, title=title, footer=footer, footer_icon=footer_icon):
                    text += f"✅ **{guild.name}**: {announce_channel}\n"
                else:
                    text += f"❌ **{guild.name}**: {announce_channel}\n"

            # Send summary message in response.
            await ctx.bot.send_embed(ctx, text=text)

    @ commands.command()
    async def vote(self, ctx):
        """ Sends a link to discord bot list """
        async with ctx.typing():
            url = "https://discordbots.org/bot/412809807842639883/vote"
            embed = discord.Embed()
            embed.description = messaging.translate(ctx, "vote", [url])
            await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(Admin(bot))
    print("Loaded Admin cog")
